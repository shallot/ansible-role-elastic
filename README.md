# Ansible Role - Elastic Agent

## Description

This is an Ansible role, designed to install Elastic Agent (the log shipper for the Elastic Stack). The role is managed by the Security Team of eyeo.

## Defaults and Variables

Below, there is a list of defaults (under 'agent/defaults/main.yml') and a list of variables (supplied via ansible directives).

### Defaults

- **elastic_agent_artifact_url:** Base URL for Elastic artifacts, which is used in downloading the specific agent artifact type
- **elastic_agent_version:** Exact artifact version to install (fleet-managed agents can be upgraded remotely, agents installed via DEB packages can't)
- **elastic_agent_fleet_artifact:** Agent string for the Elastic Fleet managed agent artifacts
- **elastic_agent_deb_artifact:** Agent string for agent artifacts with DEB installation packages

### Variables

- **elastic_agent_enrollment_token:** Token that is required to enroll the agent to the correct policy group
- **elastic_agent_install_type:** Agent enrollment type, should equal to either "fleet" or "deb"
- **elastic_agent_fleet_url:** Address for Elastic Stack cloud deployment, the agents will connect and ship logs to this address

## Testing the Role

This role can be easily tested with the provided Vagrant file. You'd naturally need the Vagrant package and a hypervisor, preferably the VirtualBox. Also you would need to supply 2 parameters to Vagrant (as in the shape of environment variables). `ES_ENROLLMENT_TOKEN` is the token from Agent Policy group created for testing purposes. `ES_AGENT_TYPE` is the installation type, should equal to either "fleet" or "deb".

There is also an optional Vagrant plugin that is used within the script. If you don't like to use it, just remove the "vagrant.env.enable" line and supply the 2 ENV variables manually. And if you'd like to install the plugin, which allows you to utilize an environment file (.env), just run the command `vagrant plugin install vagrant-env` and create an environment file with the requested variables.

After that, just run `vagrant up` or `vagrant provision` (if you already have a VM running) and check the output of the Vagrant execution.
