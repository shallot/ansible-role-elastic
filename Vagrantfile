# Copyright (c) 2023-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://www.vagrantup.com/docs/vagrantfile/vagrant_version.html
Vagrant.require_version '>= 2.0.0'

# https://www.vagrantup.com/docs/vagrantfile/version.html
Vagrant.configure(2) do |vagrant|
  vagrant.env.enable

  # https://www.vagrantup.com/docs/synced-folders/basic_usage.html#disabling
  vagrant.vm.synced_folder('.', '/vagrant', disabled: true)

  # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
  vagrant.vm.box_check_update = false
  vagrant.vm.post_up_message = nil

  # https://www.vagrantup.com/docs/multi-machine/
  # https://wiki.debian.org/DebianStretch
  vagrant.vm.define('vg-elastic-debian-test') do |config|

    # https://www.vagrantup.com/docs/vagrantfile/machine_settings.html
    config.vm.box = 'debian/bullseye64'
    config.vm.hostname = 'vg.elastic.debian.test'

    # https://www.vagrantup.com/docs/provisioning/ansible.html
    config.vm.provision('ansible') do |ansible|
      ansible.compatibility_mode = '2.0'
      ansible.verbose = 'v'
      ansible.extra_vars = {
        elastic_agent_enrollment_token: ENV['ELASTIC_AGENT_ENROLLMENT_TOKEN'],
        elastic_agent_fleet_url: ENV['ELASTIC_AGENT_FLEET_URL'],
        elastic_agent_install_type: ENV['ELASTIC_AGENT_INSTALL_TYPE'],
      }
      ansible.playbook = File.join(__dir__, 'playbook.yml')
    end

  end

end
