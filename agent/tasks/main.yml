# Copyright (c) 2023-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# https://docs.ansible.com/ansible/latest/import_role_module.html
- name:
    "assert: check enrollment token"
  assert:
    that:
      - "elastic_agent_enrollment_token is defined"
      - "elastic_agent_enrollment_token | length > 1"
      - "elastic_agent_enrollment_token != None"
    fail_msg: |
      Variable "elastic_agent_enrollment_token" is missing!"

- name:
    "assert: check install type"
  assert:
    that:
      - "elastic_agent_install_type is defined"
      - "elastic_agent_install_type | length > 1"
      - "elastic_agent_install_type != None"
      - "elastic_agent_install_type == \"fleet\" or
         elastic_agent_install_type == \"deb\""
    fail_msg: |
      Variable "elastic_agent_install_type" should equal to "fleet" or "deb"

# https://docs.ansible.com/ansible/latest/modules/shell_module.html
- name:
    "shell: check if elastic-agent already installed"
  shell:
    cmd:
      "which elastic-agent || true"
  register:
    "elastic_agent_installed"

# https://docs.ansible.com/ansible/latest/modules/include_tasks_module.html
- include_tasks:
    "{{ elastic_agent_tasks }}"
  with_first_found:
    - "os/{{ ansible_os_family | lower }}/main.yml"
    - "os/generic/main.yml"
  loop_control:
    loop_var: "elastic_agent_tasks"

# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html
- name:
    "systemd : elastic-agent"
  systemd:
    name: "{{ elastic_agent_service_name }}"
    enabled: "{{ elastic_agent_service_enabled }}"
    state: "{{ elastic_agent_service_state }}"
  become: true
